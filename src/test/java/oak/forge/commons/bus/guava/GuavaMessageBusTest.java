package oak.forge.commons.bus.guava;

import com.google.common.eventbus.*;
import oak.forge.commons.data.message.Message;
import org.junit.jupiter.api.*;

import java.time.Instant;

import static org.assertj.core.api.Assertions.*;

class GuavaMessageBusTest {

    private static final TestMessage MESSAGE = new TestMessage(17);

    @SuppressWarnings("UnstableApiUsage") // just to avoid unnecessary compiler warnings.
    private final EventBus eventBus = new EventBus();
    private final GuavaMessageBus messageBus = new GuavaMessageBus(eventBus);

    int captured = 0;

    private static final class TestMessage extends Message {
        private final int value;

        public TestMessage(int value) {
            super(Instant.now());
            this.value = value;
        }
    }

    @Test
    void should_not_handle_message_when_no_handler_registered() {
        // when
        messageBus.post(MESSAGE);

        // then
        assertThat(captured).isZero();
    }
    
    @Test
    void should_handle_message() {
        // given
        messageBus.registerMessageHandler(TestMessage.class, message -> captured = message.value + 1);

        // when
        messageBus.post(MESSAGE);

        // then
        assertThat(captured).isEqualTo(18);
    }

}